package dao;

import entities.City;
import entities.Person;
import services.ResourceService;
import stax.StAXDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySQLDao extends DAO implements PersonCityService {
    private HashMap<Integer, Integer> citiesCount = new HashMap<>();

    @Override
    public void addPerson(Person newPerson) {
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_ADD_PERSON));
            statement.setString(1, newPerson.getName());
            statement.setString(2, newPerson.getStreet());
            statement.setInt(3, newPerson.getNumber());
            statement.setInt(4, newPerson.getCity().getId());
            statement.execute();
            statement.close();
            getCitiesCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
    }

    @Override
    public int getLastInsertId(Class myClass) {
        Connection connection = poolInst.getConnection();
        int res = 0;
        try {
            String property;
            if (myClass.equals(Person.class))
                property = sql.getProperty(ResourceService.SQL_SELECT_MAX_ID_PEOPLE);
            else
                property = sql.getProperty(ResourceService.SQL_SELECT_MAX_ID_CITIES);
            PreparedStatement statement = connection.prepareStatement(property);
            ResultSet id = statement.executeQuery();
            if (id.next())
                res = id.getInt(1);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
        return res;
    }

    @Override
    public boolean isValid(Class myClass) {
        StAXDao xmlDao = new StAXDao();
        if (myClass.equals(Person.class))
            xmlDao.writePeopleInFile(getAllPeople());
        else xmlDao.writeCitiesInFile(getAllCities());
        return xmlDao.isValid(myClass);
    }


    public void addCity(City newCity) {
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_ADD_CITY));
            statement.setString(1, newCity.getName());
            statement.execute();
            statement.close();
            getCitiesCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
    }

    @Override
    public List<Person> getAllPeople() {
        Connection connection = poolInst.getConnection();
        List<Person> allPeople = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_GET_ALL_PEOPLE));
            ResultSet people = statement.executeQuery();
            while (people.next()) {
                allPeople.add(new Person(people.getInt("id"), people.getString(2),
                        people.getString("street"), people.getInt("number"), getCity(people.getInt("city_id"))));
            }
            people.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
        return allPeople;
    }

    @Override
    public void updatePerson(int id, String toUpdate, String type) {
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(String.format(sql.
                    getProperty(ResourceService.SQL_UPDATE_PERSON), type));
            if (type.equals("name") || type.equals("street")) {
                statement.setString(1, toUpdate);
            } else
                statement.setInt(1, Integer.parseInt(toUpdate));
            statement.setInt(2, id);
            statement.execute();
            statement.close();
            getCitiesCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
    }

    @Override
    public void deletePerson(int id) {
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_DELETE_PERSON));
            statement.setString(1, Integer.toString(id));
            statement.execute();
            statement.close();
            getCitiesCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
    }

    @Override
    public City getCity(int id) {
        City city = null;
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_SELECT_CITY));
            statement.setInt(1, id);
            ResultSet cities = statement.executeQuery();
            while (cities.next())
                city = new City(id, cities.getString("name"));
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
        return city;
    }

    @Override
    public List<City> getAllCities() {
        Connection connection = poolInst.getConnection();
        List<City> allCities = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_GET_ALL_CITIES));
            ResultSet cities = statement.executeQuery();
            while (cities.next()) {
                allCities.add(new City(cities.getInt("id"), cities.getString("name")));
            }
            cities.close();
            statement.close();
            getCitiesCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
        return allCities;
    }

    private void getCitiesCount() {
        Connection connection = poolInst.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql.
                    getProperty(ResourceService.SQL_SELECT_COUNT_IN_THE_CITIES));
            ResultSet count = statement.executeQuery();
            citiesCount.clear();
            while (count.next())
                citiesCount.put(count.getInt("city_id"), count.getInt("total"));
            count.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolInst.footConnection(connection);
        }
    }

    @Override
    public int getCountInTheCity(int id) {
        try {
            return citiesCount.get(id);
        } catch (NullPointerException e) {
            return 0;
        }
    }
}