package dao;

import connectionpool.DbConnectionPool;
import services.ResourceService;

public abstract class DAO {
    protected static DbConnectionPool poolInst;
    protected static ResourceService sql;

    protected DAO() {
        poolInst = DbConnectionPool.getInstance();
        sql = ResourceService.getInstance();
    }
}
