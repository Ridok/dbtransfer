package connectionpool;


import lombok.extern.slf4j.Slf4j;
import services.ResourceService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

@Slf4j
public class DbConnectionPool {
    private static DbConnectionPool instance;
    private Deque<Connection> connections;

    private DbConnectionPool() {
        this.connections = new ConcurrentLinkedDeque<>();
    }


    public synchronized Connection getConnection() {
        if (!connections.isEmpty()) {
            return connections.poll();
        }
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(ResourceService.getInstance().getProperty("url"),
                    ResourceService.getInstance().getProperty("user"), ResourceService.getInstance().getProperty("password"));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return connection;
    }


    public void footConnection(final Connection connection) {
        try {
            if (!connection.isClosed()) {
                connections.add(connection);
            }
        } catch (SQLException e) {
            log.info(e.getMessage());
        }
    }

    public static DbConnectionPool getInstance() {
        if (instance == null) {
            synchronized (DbConnectionPool.class) {
                instance = new DbConnectionPool();
                return instance;
            }
        }
        return instance;
    }
}


