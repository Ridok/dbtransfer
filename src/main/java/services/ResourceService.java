package services;

import connectionpool.DbConnectionPool;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ResourceBundle;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResourceService {
    private static ResourceService mysqlInstance;
    private static ResourceService mongoDbInstance;
    private static ResourceBundle bundle;

    private static final String SQL_FILE = "mysql";
    private static final String NO_SQL_FILE = "mongodb";

    public static final String SQL_GET_ALL_CITIES = "SQL_GET_ALL_CITIES";

    public static ResourceService getMysqlInstance() {
        if (mysqlInstance == null) {
            synchronized (DbConnectionPool.class) {
                mysqlInstance = new ResourceService();
                bundle = ResourceBundle.getBundle(SQL_FILE);
            }
        }
        return mysqlInstance;
    }

    public static ResourceService getMongoDbInstance() {
        if (mongoDbInstance == null) {
            synchronized (DbConnectionPool.class) {
                mongoDbInstance = new ResourceService();
                bundle = ResourceBundle.getBundle(NO_SQL_FILE);
            }
        }
        return mongoDbInstance;
    }

    public String getProperty(final String key) {
        return bundle.getString(key);
    }
}
